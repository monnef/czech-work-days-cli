module Main where

import Data.Monoid
import Safe (readEitherSafe)
import System.Environment
import System.Exit

import CzechWorkDays (countOfWorkdaysInMonth)

readIntOrDie :: String -> String -> IO Int
readIntOrDie msg x = do
  let res = (readEitherSafe x :: Either String Int)
  case res of Left err -> die $ msg <> err
              Right r -> return r

main :: IO ()
main = do
  args <- getArgs
  case args of
    [yearStr, monthStr] -> do
      year <- fmap fromIntegral $ readIntOrDie "Failed to parse year: " yearStr
      month <- readIntOrDie "Failed to parse month: " monthStr
      putStrLn $ show $ countOfWorkdaysInMonth year month
    _ -> die "Wrong number of arguments."
